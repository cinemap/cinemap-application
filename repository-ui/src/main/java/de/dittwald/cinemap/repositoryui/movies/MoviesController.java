/*
 * Copyright 2024 Benjamin Dittwald
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.dittwald.cinemap.repositoryui.movies;

import com.fasterxml.jackson.core.JsonProcessingException;
import de.dittwald.cinemap.repositoryui.repository.RepositoryClient;
import de.dittwald.cinemap.repositoryui.tmdb.TmdbId;
import jakarta.validation.Valid;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

@Controller
@RequestMapping("/movies")
public class MoviesController {

    private final RepositoryClient repositoryClient;

    public MoviesController(RepositoryClient repositoryClient) {
        this.repositoryClient = repositoryClient;
    }

    @GetMapping
    public String index(Model model) throws JsonProcessingException {

        List<MovieFlat> movies = new ArrayList<>(this.repositoryClient.getAllMovies());
        Collections.sort(movies);
        model.addAttribute("movies", movies);
        model.addAttribute("tmdbId", new TmdbId());

        return "movies";
    }

    @PostMapping("/delete/{movieUuid}")
    public String deleteMovie(@PathVariable UUID movieUuid) {

        this.repositoryClient.deleteMovie(movieUuid);

        return "redirect:/movies";
    }

    @PostMapping
    public String createMovieByTmdbId(@Valid @ModelAttribute TmdbId tmdbId, BindingResult result, Model model) {

        if (result.hasErrors()) {
            List<MovieFlat> movies = new ArrayList<>(this.repositoryClient.getAllMovies());
            Collections.sort(movies);
            model.addAttribute("movies", movies);

            return "movies";
        }

        // Todo: Add Error handling
        this.repositoryClient.createMovieViaTmdbId(tmdbId.getId());

        return "redirect:/movies";
    }

    @PostMapping("/add")
    public String addMovie(@Valid @ModelAttribute(name = "movie") MovieFlat movie,
                           BindingResult result, Model model) {

        if (result.hasErrors()) {
            model.addAttribute("operation", "add");
            return "movie_form";
        }

        this.repositoryClient.createMovie(movie);

        return "redirect:/movies/%s/scenes".formatted(movie.getUuid());
    }

    @PostMapping("/edit")
    public String editMovie(@Valid @ModelAttribute(name = "movie") MovieFlat movie,
                           BindingResult result, Model model) {

        if (result.hasErrors()) {
            model.addAttribute("operation", "edit");
            return "movie_form";
        }

        this.repositoryClient.updateMovie(movie);

        return "redirect:/movies/%s/scenes".formatted(movie.getUuid());
    }

    @GetMapping("/form")
    public String showMovieForm(@RequestParam(name = "movieUuid", required = false) UUID movieUuid, Model model) {

        MovieFlat movie;
        if (movieUuid == null) {
            movie = new MovieFlat();
            movie.setUuid(UUID.randomUUID());
            movie.setLocale(LocaleContextHolder.getLocale().getLanguage());
            model.addAttribute("operation", "add");
        } else {
            movie = this.repositoryClient.getMovie(movieUuid);
            model.addAttribute("operation", "edit");
        }

        model.addAttribute("movie", movie);

        return "movie_form";
    }
}
